import os
import pandas as pd
from google.cloud import bigquery
from flask import send_from_directory

import stylesheet
import js

client = bigquery.Client()
ageing_table_id = f"{os.environ['GCP_PROJECT']}.{os.environ['DATABASE_NAME']}.ageing_list"
customer_table_id = f"{os.environ['GCP_PROJECT']}.{os.environ['DATABASE_NAME']}.customer_master"
open_item_table_id = f"{os.environ['GCP_PROJECT']}.{os.environ['DATABASE_NAME']}.open_item_list"


def get_static_html():
    static = f"""
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>{stylesheet.stylesheet}</style>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script>{js.javascript}</script>
    """
    return static


def get_reference_day():
    reference_query_string = f"""SELECT ReferenceDay FROM `{open_item_table_id}` LIMIT 1"""
    reference_day = client.query(reference_query_string).result().to_dataframe(
        create_bqstorage_client=True,
    )
    return reference_day.iloc[0, 0]


def get_report():
    ageing_query_string = f"""SELECT * FROM `{ageing_table_id}`"""
    ageing_list = client.query(ageing_query_string).result().to_dataframe(
        create_bqstorage_client=True,
    )

    customer_query_string = f"""SELECT * FROM `{customer_table_id}`"""
    customer_master = client.query(customer_query_string).result().to_dataframe(
        create_bqstorage_client=True,
    )[["customer_id", "customer_name"]]
    df = pd.merge(ageing_list,
                  customer_master,
                  left_on="customer_number",
                  right_on="customer_id").drop("customer_id", axis=1).set_index("customer_name")
    df.columns = ["Customer number", "0-30 days", "31-60 days", "61-90 days", ">90 days"]
    df.index.name = "Customer name"
    df["Total"] = df[["0-30 days", "31-60 days", "61-90 days", ">90 days"]].sum(axis=1)
    df.sort_values("Total", inplace=True, ascending=False)
    return df


def serve_report(request):
    reference_day = get_reference_day()
    download = request.args.get("download")
    if download is not None:
        download = download.lower()
        if download == "true":
            sep = request.args.get("sep")
            if sep is None:
                sep = ","
            df = get_report()
            df.to_csv(f"/tmp/report-{reference_day}.csv", sep=sep)
            return send_from_directory("/tmp", filename=f"report-{reference_day}.csv", as_attachment=True)

    self_url = f"https://{os.environ['GCP_REGION']}-{os.environ['GCP_PROJECT']}.cloudfunctions.net/{os.environ['K_SERVICE']}"
    website = f"""
    <!DOCTYPE html>
    <html>
    <head>
    <title>Ageing Report</title>
    {get_static_html()}
    </head>
    <div style="padding: 25px 25px 25px 25px">
    <div style="text-align: right">
    <a class="waves-effect waves-light btn" onclick="download_report('{self_url}?download=true')"><i class="material-icons">file_download</i></a>
    </div>
    <div style="text-align: center">
    <h1>Ageing Report for Rock Retail SE</h1>
    <h3>as of {reference_day}<h3>
    </div>
    {get_report().to_html()}
    </div>
    <script>
    {js.custom_js}
    </script>

    </html>
    """

    return website


if __name__ == "__main__":
    print(serve_report(""))
