
resource "google_storage_bucket_object" "ageingreport-source" {
  name = "ageingreport-${var.zip-checksums["ageingreport-function"]}.zip"
  bucket = var.functions-bucket
  source = "${var.files}/ageingreport.zip"
}