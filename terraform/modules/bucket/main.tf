
resource "google_storage_bucket" "cloud-functions-source" {
  name = "lob-${var.stage-symbol}-usc1-gs-functions-01"
  location = var.location
  force_destroy = true
}