
resource "google_bigquery_dataset" "alteryx" {
  dataset_id                  = "alteryx"
  friendly_name               = "alteryx"
  description                 = "Database for customer data from alteryx"
  location                    = "US"
  delete_contents_on_destroy  = true
}
