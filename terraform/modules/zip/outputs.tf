output "zip-checksums" {
  value = {
    "ageingreport-function" = data.archive_file.ageingreport.output_sha
  }
}