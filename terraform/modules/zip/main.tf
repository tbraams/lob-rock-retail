locals {
  ageingreport-files = [
    "${var.functions}/ageing_report/main.py",
    "${var.functions}/ageing_report/js.py",
    "${var.functions}/ageing_report/stylesheet.py",
    "${var.functions}/ageing_report/requirements.txt",
  ]
}

data "template_file" "ageingreport_file" {
  count = length(local.ageingreport-files)
  template = file(element(local.ageingreport-files, count.index))
}

data "archive_file" "ageingreport" {
  type = "zip"
  output_path = "../../files/ageingreport.zip"

  source {
    content  = data.template_file.ageingreport_file.0.rendered
    filename = basename(local.ageingreport-files[0])
  }

  source {
    content  = data.template_file.ageingreport_file.1.rendered
    filename = basename(local.ageingreport-files[1])
  }

  source {
    content  = data.template_file.ageingreport_file.2.rendered
    filename = basename(local.ageingreport-files[2])
  }

  source {
    content  = data.template_file.ageingreport_file.3.rendered
    filename = basename(local.ageingreport-files[3])
  }
}