
resource "google_cloudfunctions_function" "ageingreport-function" {
  name   = "ageingreport"
  runtime = "python39"
  available_memory_mb = 256
  max_instances = 50
  source_archive_bucket = var.functions-source-bucket
  source_archive_object = var.file-reference["ageingreport-function"]
  trigger_http = true
  entry_point = "serve_report"
  environment_variables = {
    GCP_PROJECT = var.project
    GCP_REGION = var.region
    DATABASE_NAME = var.database_name
  }
}

# IAM entry for all users to invoke the function
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.ageingreport-function.project
  region         = google_cloudfunctions_function.ageingreport-function.region
  cloud_function = google_cloudfunctions_function.ageingreport-function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}