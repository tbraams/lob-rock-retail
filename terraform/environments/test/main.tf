
provider "google" {
  credentials = file("service-account.json")
  project = var.PROJECT
  region = var.LOCATION
  zone = "${var.LOCATION}-b"
}

terraform {
  backend "gcs" {
    bucket = "lob-t-usc1-gs-tfstate-01"
    prefix = "terraform/state"
  }
}

module "bucket" {
  source = "../../modules/bucket"
  location = var.LOCATION
  stage-symbol = var.STAGE_SYMBOL
}

module "zip" {
  source = "../../modules/zip"
  functions = "../../../cloudfunction"
}

module "files" {
  source = "../../modules/files"
  files = "../../files"
  functions-bucket = module.bucket.functions-source-bucket
  project = var.PROJECT
  zip-checksums = module.zip.zip-checksums
}

module "bigquery" {
  source = "../../modules/bigquery"
}

module "functions" {
  source = "../../modules/functions"
  file-reference = module.files.file-reference
  functions-source-bucket = module.bucket.functions-source-bucket
  project = var.PROJECT
  region = var.LOCATION
  database_name = module.bigquery.bq-dataset
}