
variable "PROJECT" {
  default = "fsgcp2"
}

variable "LOCATION" {
  default = "us-central1"
}

variable "STAGE_SYMBOL" {
  default = "t"
}